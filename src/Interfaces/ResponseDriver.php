<?php

namespace Multithread\Interfaces;

/**
 * Interface ResponseDriver
 * @package Interfaces
 */
interface ResponseDriver
{
    /**
     * @param $response
     *
     * @return mixed
     */
    public function saveResponse($response);

    /**
     * @return \Generator
     */
    public function getResponses(): \Generator;

    /**
     * @return void
     */
    public function registerShutdown(): void;
}
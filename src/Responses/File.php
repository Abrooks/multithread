<?php

namespace Multithread\Responses;

use Multithread\Interfaces\ResponseDriver;
use Multithread\Thread;

/**
 * Class File
 * @package Responses
 */
class File implements ResponseDriver
{
    /**
     * @var string
     */
    public static $saveLocation = __DIR__ . '/.responses';

    /**
     * @var string
     */
    public static $endOfLineCharacter = ',';

    /**
     * @inheritDoc
     */
    public function saveResponse($response)
    {
        $f = fopen(static::$saveLocation, 'a');
        while (true) {
            $hasLock = flock($f, LOCK_EX);
            if ($hasLock) {
                fwrite($f, serialize($response) . static::$endOfLineCharacter . PHP_EOL);
                fclose($f);
                break;
            }
        }
    }

    /**
     * @inheritDoc
     */
    public function getResponses(): \Generator
    {
        $f = fopen(static::$saveLocation, 'r');
        if ($f) {
            while ( ! feof($f)) {
                $data = $this->getSerializedLine($f);
                preg_replace('/,' . PHP_EOL . '$/', '', $data);
                yield unserialize($data);
            }
        }
        fclose($f);
    }

    /**
     * @param $file
     *
     * @return string
     */
    public function getSerializedLine($file)
    {
        $line = '';
        do {
            $line      .= fgets($file);
            $cleanLine = str_replace(PHP_EOL, '', $line);
            $length    = mb_strlen($cleanLine);
        } while ( ! feof($file) && substr($cleanLine, $length - 1, $length) !== static::$endOfLineCharacter);

        return $line;
    }

    /**
     *
     */
    public function removeFile()
    {
        if (Thread::$pid) {
            if (is_file(static::$saveLocation) && is_readable(static::$saveLocation)) {
                unlink(static::$saveLocation);
            }
        }
    }

    /**
     * @inheritDoc
     */
    public function registerShutdown(): void
    {
        register_shutdown_function([$this, 'removeFile']);
    }
}

<?php

namespace Multithread;

use Multithread\Interfaces\ResponseDriver;
use Multithread\Responses\File;

/**
 * Class Thread
 * @package Multithread
 */
abstract class Thread
{
    /**
     * @var ResponseDriver
     */
    public static $responseDriver = File::class;
    /**
     * @var int
     */
    public static $run = 1;
    /**
     * @var int
     */
    public static $pid;
    /**
     * @var array
     */
    public $processIds = [];
    /**
     * @var int
     */
    public $processes = 0;
    /**
     * @var ResponseDriver
     */
    public $driver;

    /**
     * @param int $maxProcesses
     *
     * @return ResponseDriver
     */
    public function start($maxProcesses = 1): ResponseDriver
    {
        $data         = true;
        $this->driver = new static::$responseDriver;
        $this->driver->registerShutdown();
        do {
            for ($i = $this->processes; $i < $maxProcesses; $i++) {
                $data = $this->dataGetter();
                if ($data) {
                    static::$pid = pcntl_fork();
                    if (static::$pid) {
                        // main
                        $this->processes++;
                        $this->processIds[] = static::$pid;
                        self::$run++;
                    } else {
                        $response = $this->run($data);
                        $this->driver->saveResponse($response);
                        exit();
                    }
                }
            }

            pcntl_wait($status);
            $this->processes--;
        } while ($this->processes < $maxProcesses && $data);

        return $this->driver;
    }

    /**
     * @return mixed
     */
    abstract public function dataGetter();

    /**
     * @param $arguments
     *
     * @return mixed
     */
    abstract protected function run($arguments);

    /**
     *
     */
    public function __destruct()
    {
        $this->waitForCompletion();
    }

    public function waitForCompletion()
    {
        while (pcntl_waitpid(0, $status) != -1) {
        }
    }
}

<?php

require_once __DIR__ . "/../vendor/autoload.php";

class imageFormatter extends \Multithread\Thread
{
    public $images;

    public function __construct()
    {
        $count = 0;
        echo 'Generating Data...' . PHP_EOL;
        do {
            $file = __DIR__ . '/images/' . $count . '.jpg';
            if (!is_file($file)) {
                file_put_contents($file, file_get_contents('http://placekitten.com/' . ($count * 10) . '/' . ($count * 10)));
            } else {
                $this->images[] = $file;
            }
            $count++;
        } while ($count <= 200);
        echo 'Data Generated' . PHP_EOL;
    }

    public function dataGetter()
    {
        if (isset($this->images[self::$run])) {
            return $this->images[self::$run - 1];
        }

        return false;
    }

    public function run($arguments)
    {
        $img = new Imagick();
        $img->readImage($arguments);
        $img->setCompressionQuality(90);
        $img->setImageFormat('png');
        $img->writeImage(__DIR__ . '/images/' . self::$run . '.png');

        return 'Image written ' . self::$run . PHP_EOL;
    }
}

$start     = microtime(true);
$task      = new imageFormatter();
$responses = $task->start(20);
$end       = microtime(true);
$task->waitForCompletion();

echo 'time taken: ' . ($end - $start) . 's' . PHP_EOL;

foreach ($responses->getResponses() as $response) {
    print_r($response);
}
